from .base_conf import Base


class Dev(Base):
    ENVIRONMENT = 'local'
    DEBUG = True
    HOST = '0.0.0.0'
    PORT = 3000
