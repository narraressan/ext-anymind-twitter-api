import os
from .develop import Dev
'''
MOUNTED_CONFIG -- by default, use `Dev` config, we don't support production code on this project
'''
MOUNTED_CONFIG = Dev
