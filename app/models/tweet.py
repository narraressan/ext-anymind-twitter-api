from flask_restplus import Model, fields
"""
Flask-RESTplus Models provides a convenient way to define fields using json format

Note:
    if you are looking to transform the output of the wrapper, you may do so here.

SPECIAL NOTE:
    in the exam document, the response are not exactly the same with the current format from the API.
    I suspect that some changes has happened or the version of the API is different.
    To move forward, I will return the current supported format
"""

schema = {
    'created_at': fields.DateTime(dt_format='rfc822'),
    'id': fields.Integer(),
    'id_str': fields.String(),
    'text': fields.String(),
    'truncated': fields.Boolean(),
    'entities': fields.Raw(),
    'metadata': fields.Raw(),
    'source': fields.String(),
    'user': fields.Raw(),
    'geo': fields.Raw(),
    'coordinates': fields.Raw(),
    'place': fields.Raw(),
    'contributors': fields.Raw(),
    'retweeted_status': fields.Raw(),
    'is_quote_status': fields.Boolean(),
    'retweet_count': fields.Integer(),
    'favorite_count': fields.Integer(),
    'favorited': fields.Boolean(),
    'retweeted': fields.Boolean(),
    'lang': fields.String(),

    # to show that I have customized the response, I have omitted several keys which I findnot very informative
    # omitted keys:
    #     in_reply_to_status_id,
    #     in_reply_to_status_id_str,
    #     in_reply_to_user_id,
    #     in_reply_to_user_id_str,
    #     in_reply_to_screen_name
}

Tweet = Model('Tweet', schema)
