from flask import request
from flask_restplus import Namespace, Resource, fields

from twitter import TwitterAPI
from models import Tweet

api = Namespace('Timelines', description='docs - https://developer.twitter.com/en/docs/tweets/timelines/api-reference/get-statuses-user_timeline')
api.models[Tweet.name] = Tweet
""" spawn my own twitterAPI middleman """
twitterAPI = TwitterAPI()


@api.route('/<string:username>')
class GetUserTweets(Resource):
    """
    search all recent tweets from this user

    args:
        username (required) -- forwarded to twitter api and expected to be a twitter username

    params:
        limit -- request parameter to limit results. Default is set to 30
    """
    @api.marshal_with(Tweet, as_list=True)
    @api.expect(api.parser().add_argument('limit', type=int, default=30, help='Limit of response returned per batch'))
    def get(self, username=None):
        return twitterAPI.get_user_tweets(username, request.args)
