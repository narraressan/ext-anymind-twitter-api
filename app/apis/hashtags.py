from flask import request
from flask_restplus import Namespace, Resource, fields

from twitter import TwitterAPI
from models import Tweet

api = Namespace('Hashtags', description='docs - https://developer.twitter.com/en/docs/tweets/search/api-reference/get-search-tweets')
api.models[Tweet.name] = Tweet
""" spawn my own twitterAPI middleman """
twitterAPI = TwitterAPI()


@api.route('/<string:hashtag>')
class SearchHashtags(Resource):
    """
    search for tweets with specific hashtags

    args:
        hashtag (required) -- forwarded to twitter api and is expected to be a hashtag

    params:
        limit (optional) -- used to limit results. Default is set to 30
    """
    @api.marshal_with(Tweet, as_list=True)
    @api.expect(api.parser().add_argument('limit', type=int, default=30, help='Limit of response returned per batch'))
    def get(self, hashtag=None):
        return twitterAPI.find_hashtags(hashtag, request.args)
