import logging
from logging.handlers import RotatingFileHandler
""" define a very simple logging function """
logging.basicConfig(level=logging.DEBUG, handlers=[RotatingFileHandler('logs.log', maxBytes=(1048576 * 5), backupCount=7), logging.StreamHandler()])
log = logging.getLogger()
