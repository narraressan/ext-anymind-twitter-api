"""
reference:
    https://werkzeug.palletsprojects.com/en/0.15.x/exceptions/
"""

from werkzeug.exceptions import HTTPException


class TwitterWrapperBaseException(HTTPException):
    code = 500
    error = None

    def __init__(self, msg=None):
        self.data = {'error': self.error, 'message': msg}

        HTTPException.__init__(self, description=self.data)


class QueryFailed(TwitterWrapperBaseException):
    error = 'QUERY_FAILED'
    code = 500


class NoHashtagFound(TwitterWrapperBaseException):
    error = 'NO_HASHTAG_FOUND'
    code = 401


class NoTweetsFound(TwitterWrapperBaseException):
    error = 'NO_TWEETS_FOUND'
    code = 401


class NoUserFound(TwitterWrapperBaseException):
    error = 'NO_USER_FOUND'
    code = 401
