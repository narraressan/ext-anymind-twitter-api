import os, tempfile, pytest, random, string, json
from app import app


@pytest.fixture
def client():
    app.config['TESTING'] = True

    with app.test_client() as client:
        yield client


def test_search_hashtags(client):
    hashtags = ['MMA', 'VEGAN', 'OOTD', 'TravelTuesday', 'TBH']

    for tag in hashtags:
        resp = client.get('/hashtags/{}'.format(tag), query_string={'limit': 10})
        data = json.loads(resp.data)

        assert len(data) > 0
        assert resp.status_code == 200


def test_search_invalid_hashtags(client):
    invalid_hashtags = ['this_hashtag_should_not_exist', 'randomwords_for_hashtag']

    for tag in invalid_hashtags:
        resp = client.get('/hashtags/{}'.format(tag), query_string={'limit': 10})
        data = json.loads(resp.data)

        assert len(data) == 0
        assert resp.status_code == 200


def test_fetch_user_tweets(client):
    users = ['adeanladia', 'twitterapi', 'adean', 'ellen', 'shakira', 'Rihanna']

    for user in users:
        resp = client.get('/users/{}'.format(user), query_string={'limit': 10})
        data = json.loads(resp.data)

        assert len(data) > 0
        assert resp.status_code == 200


def test_fetch_invalid_users(client):

    users = ['thisysershouldntexist', 'asadafgwere', 'hello_world_this_is_a_weird_name']

    for user in users:
        resp = client.get('/users/{}'.format(user), query_string={'limit': 10})
        data = json.loads(resp.data)

        assert data['error'] == 'QUERY_FAILED'
        assert resp.status_code == 500
