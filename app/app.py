from config import MOUNTED_CONFIG
from setup import init, extend

app = init(MOUNTED_CONFIG)
extend(app)

if __name__ == '__main__':
    app.run(host=app.config.get("HOST"), port=app.config.get("PORT"), debug=app.config.get("DEBUG"))
