from flask import Flask, Blueprint
from flask_restplus import Resource, Api
from flask_restplus.apidoc import apidoc
from flask_cors import CORS

from apis import hashtags, users


def extend(app):
    """ setup all the dependencies for Flask-restplus """
    CORS(app, resources={r"*": {"origins": "*"}})

    blueprint = Blueprint('api', __name__)
    api = Api(blueprint)
    api.add_namespace(hashtags, path='/hashtags')
    api.add_namespace(users, path='/users')

    app.register_blueprint(blueprint)


def init(config):
    """ initialize the Flask application """
    app = Flask(__name__)
    app.url_map.strict_slashes = False
    app.config.from_object(config)

    return app
