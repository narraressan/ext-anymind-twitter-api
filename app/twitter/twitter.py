import json, urllib, oauth2 as oauth
from utils import log, QueryFailed, NoHashtagFound
from config import MOUNTED_CONFIG as conf


class TwitterAPI(object):
    def __init__(self):
        log.info('\n\nCurrent API Keys mounted...')
        log.info('CONSUMER_KEY: {}'.format(conf.CONSUMER_KEY))
        log.info('CONSUMER_SECRET: {}'.format(conf.CONSUMER_SECRET))
        log.info('ACCESS_KEY: {}'.format(conf.ACCESS_KEY))
        log.info('ACCESS_SECRET: {}'.format(conf.ACCESS_SECRET))
        log.info('\n')

        self.consumer = oauth.Consumer(key=conf.CONSUMER_KEY, secret=conf.CONSUMER_SECRET)
        self.access_token = oauth.Token(key=conf.ACCESS_KEY, secret=conf.ACCESS_SECRET)
        self.client = oauth.Client(self.consumer, self.access_token)
        self.base_uri = 'https://api.twitter.com/1.1/{}'

    def find_hashtags(self, hashtag=None, filters={}):
        tweets = None

        # according to docs, 15 is default and max is 100
        limit = filters.get('limit', 30)

        if hashtag is not None and hashtag.strip() != '':
            """
            reference docs at
                - https://developer.twitter.com/en/docs/tweets/search/api-reference/get-search-tweets
            """
            clean_hashtag = urllib.parse.quote('#{}'.format(hashtag))
            uri = 'search/tweets.json?q={}&count={}'.format(clean_hashtag, limit)
            resp, data = self.execute_request(uri)

            if resp.status == 200:
                tweets = data['statuses']
            else:
                raise QueryFailed('Twitter API returned status {}'.format(resp.status))

        else:
            """ missing hashtag, prompt error """
            raise NoHashtagFound('No valid hashtag found.')

        return tweets

    def get_user_tweets(self, username=None, filters={}):
        tweets = None

        # according to docs, 15 is default and max is 100
        limit = filters.get('limit', 30)

        if username is not None and username.strip() != '':
            """
            reference docs at
                - https://developer.twitter.com/en/docs/tweets/timelines/api-reference/get-statuses-user_timeline
            """
            clean_name = urllib.parse.quote(username)
            uri = 'statuses/user_timeline.json?screen_name={}&count={}'.format(clean_name, limit)
            resp, data = self.execute_request(uri)

            if resp.status == 200:
                tweets = data
            else:
                raise QueryFailed('Twitter API returned status {}'.format(resp.status))

        else:
            """ missing hashtag, prompt error """
            raise NoHashtagFound('No valid hashtag found.')

        return tweets

    def execute_request(self, url):
        endpoint = self.base_uri.format(url)

        resp, tmp = self.client.request(endpoint)
        data = json.loads(tmp)

        log.info('forwarding-request-to:' + endpoint)
        return (resp, data)
