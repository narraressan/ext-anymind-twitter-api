## ext-anymind-twitter-api

> A simple Flask-RESTplus application using `twitter-api` as main data source


### Stack
- python 3.7
- Flask-RESTplus
- pytest
- oauth2

##### More info
- yapf - for linting
- flask-cors - for managing CORS specifics
- docker - this application can run with dockers as well. Refer to docker section for how to.
- gunicorn - by default, I have set docker-compose to run the app with gunicorn
- gitlab-ci - a simple `.gitlab-ci.yaml` is ready for basic pipeline integration.

##### Twitter API Credentials

For this application, I have secured myself a personal twitter app credentials.
By default, I have set `config/base.py`, `gitlab-ci.yml`, `docker-compose.yml` to use my personal keys, however, if you would like to try different keys, simply set in environment variables the following

```bash
set TWITTER_CONSUMER_KEY=new-cons-key
set TWITTER_CONSUMER_SECRET=new-cons-secret
set TWITTER_ACCESS_KEY=new-acc-key
set TWITTER_ACCESS_SECRET=new-acc-secret
```

> Note: setting environment variables may vary depending on your OS


### Setup local dev

I highly suggest to use `virtualenv` for your local dev environment, but if you prefer not, skip to the next step

> Note: I am using windows OS. Commands for installing and activating virtualenv may vary depending on OS.

``` bash
# setup vritualenv
pip install virtualenv
virtualenv env

# activate env, always do this before running the application
.\env\Scripts\activate.bat
```

Install libraries and other dependencies.

```bash
# install dependencies
pip install -r requirements.txt

# go to app directory
cd ./app

# serve with hot reload at localhost:3000
# swagger at localhost:3000
python app.py
```

Run unit tests with `pytest`. This stage is also included in the gitlab-ci setup.

```bash
# to run tests, simply execute
# you should be inside `./app` directory
pytest tests -s
```

Manual testing can be done with any http request tools. A curl command is written below for reference. Also, you can checkout `localhost:3000/` on your prefered browser for the Swagger tool

```bash
# search for `mma` hashtags
curl -X GET http://localhost:3000/hashtags/mma?limit=1

# view tweets in user `adeanladia`
curl -X GET http://localhost:3000/users/adeanladia?limit=10
```

Yapf follows `pep8` by default. To check and fix your code, simply follow the instructions below.

Linting check is also included in gitlab-ci setup.

```bash
# VIEW all python files with wrong linting formats
yapf --recursive . --exclude .\env\* --diff

# FIX python files with wrong linting formats
yapf --recursive . --exclude .\env\* --in-place
```


### Run local with Docker

``` bash
# build with docker
docker-compose build

# make sure you start docker on your local
# this setup also supports hot reload
docker-compose up -d

# check if everything is running
docker ps -a

# view and follow last 10 lines of the log
docker-compose logs -f --tail 10

# stop docker
docker-compose down
```

> Note:
>   make sure you have shared your local drive in docker settings, and exposed port 3000 for public use

> Special Notes:
>   There are cases where the app is unreachable if you run `python app.py` after you have run your docker machine.
>   This is because requests to port 3000 (or whatever port you use) will be sent first to the docker-machine instead of to the app - even if you didn't start the application in docker.
>   This case is NOT the application's fault, rather the docker. You can simply execute `docker-machine stop` to stop the docker engine.


### Short tour

> Note: I have opted to design this application in the most minimalist structure I could think in a single days time.

```bash
ROOT: ext-anymind-twitter-ap
/env                        # this will be auto generated if you use virtualenv
/app
    /apis
        - __init__
        - hashtags.py       # contains API definition for `/hashtags/<hashtag>`
        - users.py          # contains API definition for `/users/<username>`

    /config
        - __init__
        - base.py
        - dev.py

    /models
        - __init__
        - tweet.py          # a model to marshal the response out of the wrapper API

    /tests
        - __init__
        - test_twitter_api_wrapper.py      # unit testing with pytest
        - initial_api.py                   # this is the prototype

    /twitter
        - __init__
        - twitter.py          # handles the exchange between Twitter API and this app

    /utils
        - __init__
        - exceptions.py     # some application specific exceptions
        - logging.py

- .gitlab-ci.yml            # this defines the gitlab CI pipelines
- .style.yapf               # a simple changes I want over pep8 standards
- docker-compose.yml        # docker-compose config file. This setup readily supports hot reload.
- Dockerfile                # docker images config file
- README.md
- requirements.txt          # list of python requirements
```


### Disclaimer

To follow `twitter app` rules, I have NOT included any production ready code in this project. This setup is only for local development, personal learning, and exploration.

For this project, I have not used any 3rd party libraries or SDK to access twitter api, instead, I utilized the publicly available REST service which can be found [here](https://developer.twitter.com/en/docs).